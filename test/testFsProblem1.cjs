const fsProblem1 = require('../fs-problem1.cjs');
const path = require("path")

const absolutePathOfRandomDirectory = path.join(__dirname, 'random_files');
const randomNumberOfFiles = 3;


fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles);