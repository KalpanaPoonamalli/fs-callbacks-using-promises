const fs = require("fs")

function fsProblem2(filePath){


    const readingFile = new Promise((resolve, reject) => {
        fs.readFile(filePath, "utf-8", (err, data) => {
            if(err){
                reject("Error in reading lipsumFile: ", err)
            } else {
                console.log("LipsumFile readed successfully")
                resolve("LipsumFile readed successfully", data)
            }
        })
    })

    function creatingFile(upperCaseData){
        return new Promise((resolve, reject) => {
            fs.writeFile('newFile.txt', upperCaseData, (err, data) => {
                if(err){
                    reject("Error in creating File: ", err)
                } else {
                    console.log("Created newFile.txt")
                    resolve("Created newFile.txt", data)
                }
            })
        })
    }

    function appendingFiles(){
        return new Promise((resolve, reject) => {
            fs.appendFile("filenames.txt", "newFile.txt" + "\n", (err) => {
                if(err){
                    reject("Error in appending Files: ", err)
                } else {
                    console.log("Files apended to newfile")
                    resolve("Files apended to newfile")
                }
            })
        })

    }

    function readingLowerCaseData(){
        return new Promise((resolve, reject) => {
        fs.readFile('newFile.txt', 'utf-8', (err, data) =>{
            if(err){
                reject("Error in reading Lower Case Data: ", err)
            } else {
                const lowerCaseData = data.toLowerCase()
                console.log(lowerCaseData)
                const splittinTosentances = lowerCaseData.split(/[.!?]/).filter(Boolean);
                console.log(splittinTosentances)

                const sortingSplittedData = splittinTosentances.sort()
                resolve(sortingSplittedData)
            }
        })
    })
}

    function creatingSortFile(sortedData) {

        return new Promise((resolve, reject) => {
            fs.writeFile("sortedDataFile.txt", sortedData.join("\n"), (err, data) => {
                if(err){
                    reject("Error in creating sorted file", err)
                } else {
                    resolve("sortedDataFile.txt")
                    console.log("created sorted file")
                }
            })
        })
        
    }

    function appendingSortedDataFile(sortedFilename){
        return new Promise((resolve, reject) => {
            fs.appendFile('filenames.txt', sortedFilename+ "\n", (err, data) => {
                if(err){
                    reject("Error in appending Sorted Data file", err)
                } else {
                    console.log("sorted file name added to filenames.txt")
                    resolve("sorted file name added to filenames.txt", data)
                }
            })
        })
    }


    function readfileNames(){
        return new Promise((resolve, reject) => {
            fs.readFile('filenames.txt', 'utf-8', (err, data) =>{
                if(err){
                    reject("Error in reading filenames.txt: ", err)
                } else {
                    const filenamesList = data.trim().split("\n")
                    console.log("filenames to delete")
                    console.log(filenamesList);

                    resolve(filenamesList)
                }
            })
        })
    }


   function deletingFiles(filenames){
        return new Promise((resolve, reject) => {
            filenames.map((file) => {
                fs.unlink(file, (err) => {
                    if(err){
                        reject("Error in deleting filenames")
                    } else {
                        resolve(`${file} deleted Successfully.`)
                    }
                })
            })
        })
   }




    
    readingFile.then(result1 => {
        console.log(result1)
        const upperCaseData = result1.toUpperCase()
        return creatingFile(upperCaseData)
    })  
    .then(result2 => {
        console.log(result2)
        return appendingFiles()
    })
    .then(result3 => {
        console.log(result3)
        return readingLowerCaseData()
    })
    .then(sortedData =>{
        console.log(sortedData)
        return creatingSortFile(sortedData)
    })
    .then(sortedFilename => {
        console.log(sortedFilename)
        return appendingSortedDataFile(sortedFilename)
    })
    .then(appendingSortedFilename => {
        console.log(appendingSortedFilename)
        return readfileNames()
    })
    .then(filenamesToDelete => {
        console.log(filenamesToDelete)
        return deletingFiles(filenamesToDelete)
    })
    .then(deletedFiles => {
        console.log(deletedFiles)
    })
    .catch(err => console.error(err))

}


module.exports = fsProblem2;