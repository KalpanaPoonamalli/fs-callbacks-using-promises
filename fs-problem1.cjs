const fs = require("fs");

function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles){


  const creatingDirectory =  new Promise((resolve, reject) => {

    fs.mkdir(absolutePathOfRandomDirectory, (err) => {
        if(err){
            reject("Error in creating Directory: ", err)
        } else {
            //console.log("Directory created successfully.")
            resolve("Directory created successfully.")
        }
    })

  })  

 const creatingFiles = new Promise((resolve, reject) => {
    for (let index=1; index<= randomNumberOfFiles; index++){
        fs.writeFile(`file${index}.txt`, `This is file${index} data` , (err) => {
            if(err){
                reject("Error in Creating file: ", err)
            } else {
                console.log(`file${index}.txt created successfully.`)
                resolve("files created")
            }
        })
    }
 })


 const deletingFiles = new Promise((resolve, reject) => {
    for(let index1=1; index1<=randomNumberOfFiles; index1++){
        fs.unlink(`file${index1}.txt`, (err) => {
            if(err){
                reject("Error in deleting files: ", err)
            } else {
                console.log(`file${index1}.txt deleted successfully`)
                resolve("files deleted")
            }
        })
    }
 })

 const deletingDirectory = new Promise((resolve, reject) => {
    fs.rmdir(absolutePathOfRandomDirectory, (err) => {
        if(err){
            reject("Error in deleting Directory: ", err)
        } else {
            resolve("Directory Deleted Successfully.")
        }
    })
 })


 creatingDirectory.then((result1) => {
    console.log(result1)
    return creatingFiles
 })
 .then((result2) => {
    console.log(result2)
    return deletingFiles
 })
 .then((result3) => {
    console.log(result3)
    return deletingDirectory
 })
 .then((result4) => {
    console.log(result4)
 }) 
 .catch(err => {
    console.error(err)
 })

}
  

module.exports = fsProblem1;